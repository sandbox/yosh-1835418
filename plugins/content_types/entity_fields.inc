<?php

/**
 * @file
 * Plugin to handle the 'entity_fields' content type which shows multiple fields of an entity.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title'        => t('Multiple fields'),
  'single'       => TRUE,
  'icon'         => 'icon_node.png',
  'description'  => t('Display multiple fields.'),
  'category'     => t('Node'),
  'all contexts' => TRUE,
  'edit form'    => array(
    'ctools_entity_fields_entity_fields_content_type_fields' => array(
      'default' => TRUE,
      'title'   => t('Configure multiple fields'),
    ),
    'ctools_entity_fields_entity_fields_content_type_formatter_options' => array(
      'title'   => t('Formatter options'),
    ),
    'ctools_entity_fields_entity_fields_content_type_formatter_settings' => array(
      'title'   => t('Formatter settings'),
    ),
  ),
  'defaults'     => array(
    'fields'  => array(),
    'subtype' => NULL,
  ),
);

/**
 * Fields form.
 */
function ctools_entity_fields_entity_fields_content_type_fields($form, &$form_state) {
  // Get info about all known entity types.
  $entities = entity_get_info();

  // Fields live in an array.
  $form['fields'] = array();

  // Loop through contexts.
  foreach ($form_state['contexts'] as $context_id => $context) {
    // Loop through entities.
    foreach ($entities as $entity_type => $entity) {
      // Ensure that entity type matches context.
      if ($entity_type != $context->type[2]) {
        continue;
      }

      // Loop through entity bundles.
      foreach ($entity['bundles'] as $bundle_type => $bundle) {
        // Ensure that bundle type matches context.
        if (!empty($context->restrictions['type']) && !in_array($bundle_type, $context->restrictions['type'])) {
          continue;
        }

        // Loop through fields for entity bundle.
        foreach (field_info_instances($entity_type, $bundle_type) as $field_name => $field) {
          // Create id.
          $id = $context_id . '__' . $field_name;

          // Add field.
          $form['fields'][$id] = array(
            'context_id' => array(
              '#type'  => 'value',
              '#value' => $context_id,
            ),
            'entity_type' => array(
              '#type'  => 'value',
              '#value' => $entity_type,
            ),
            'bundle_type' => array(
              '#type'  => 'value',
              '#value' => $bundle_type,
            ),
            'field_name' => array(
              '#type'  => 'value',
              '#value' => $field_name,
            ),
            'field_label' => array(
              '#type'  => 'value',
              '#value' => t('!field (from !context)', array('!field' => $field['label'], '!context' => $context->identifier)),
            ),
            'field_label_markup' => array(
              '#type'   => 'markup',
              '#markup' => t('!field (from !context)', array('!field' => $field['label'], '!context' => $context->identifier)),
            ),
            'enabled' => array(
              '#type'          => 'checkbox',
              '#default_value' => isset($form_state['conf']['fields'][$id]['enabled']) ? $form_state['conf']['fields'][$id]['enabled'] : 0,
            ),
            'weight' => array(
              '#type'          => 'weight',
              '#default_value' => isset($form_state['conf']['fields'][$id]['weight']) ? $form_state['conf']['fields'][$id]['weight'] : 0,
            ),
          );
        }
      }
    }
  }

  // Sort fields by weight.
  uasort($form['fields'], function ($a, $b) { return (int) $a['weight']['#default_value'] - (int) $b['weight']['#default_value']; });

  // Fields are tree huggers.
  $form['fields']['#tree'] = TRUE;

  // Add subtype.
  $form['subtype'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Subtype'),
    '#description'   => t('The subtype can be used by other modules or themes to identify this block of content.'),
    '#size'          => 30,
    '#default_value' => !empty($form_state['conf']['subtype']) ? $form_state['conf']['subtype'] : '',
  );

  return $form;
}

/**
 * Submit fields form.
 */
function ctools_entity_fields_entity_fields_content_type_fields_submit($form, &$form_state) {
  // Store in conf.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }

  // Sort fields by weight.
  uasort($form_state['conf']['fields'], function ($a, $b) { return (int) $a['weight'] - (int) $b['weight']; });
}

/**
 * Theme fields form.
 */
function theme_ctools_entity_fields_entity_fields_content_type_fields($variables) {
  $form = $variables['form'];

  // Build table header.
  $header = array(t('Field'), t('Enabled'), t('Weight'));

  // Build rows.
  $rows = array();
  foreach (element_children($form['fields']) as $id) {
    // Get field as reference.
    $field = &$form['fields'][$id];

    // Add class.
    $field['weight']['#attributes']['class'] = array('weight');

    // Build row.
    $row = array(
      drupal_render($field['field_label_markup']),
      drupal_render($field['enabled']),
      drupal_render($field['weight']),
    );

    // Add row.
    $rows[] = array('data' => $row, 'class' => array('draggable'));
  }

  // Build table.
  $form['fields'] = array(
    '#theme'      => 'table',
    '#header'     => $header,
    '#rows'       => $rows,
    '#attributes' => array('id' => 'entity-fields-table'),
  );

  // Make table draggable.
  drupal_add_tabledrag('entity-fields-table', 'order', 'sibling', 'weight');

  return drupal_render_children($form);
}

/**
 * Formatter options form.
 */
function ctools_entity_fields_entity_fields_content_type_formatter_options($form, &$form_state) {
  // Fields live in an array.
  $form['fields'] = array();

  // Fields are tree huggers.
  $form['fields']['#tree'] = TRUE;

  // Loop through enabled fields.
  foreach ($form_state['conf']['fields'] as $id => $field) {
    // Ensure that field is enabled.
    if ($field['enabled']) {
      // Get field info.
      $field_info = field_info_field($field['field_name']);

      // Create fieldset.
      $form['fields'][$id] = array(
        '#type'  => 'fieldset',
        '#title' => $field['field_label'],
      );

      // Get label options.
      $label_options = array(
        'above'  => t('Above'),
        'inline' => t('Inline'),
        'hidden' => t('Hidden'),
      );

      // Add label selector.
      $form['fields'][$id]['field_label_display'] = array(
        '#type'          => 'select',
        '#title'         => t('Label'),
        '#options'       => $label_options,
        '#default_value' => !empty($form_state['conf']['formatter_options'][$id]['field_label_display']) ? $form_state['conf']['formatter_options'][$id]['field_label_display'] : NULL,
      );

      // Get formatter options.
      module_load_include('inc', 'field_ui', 'field_ui.admin');
      $formatter_options = field_ui_formatter_options($field_info['type']);

      // Add formatter selector.
      $form['fields'][$id]['field_formatter'] = array(
        '#type'          => 'select',
        '#title'         => t('Select a formatter'),
        '#options'       => $formatter_options,
        '#default_value' => !empty($form_state['conf']['formatter_options'][$id]['field_formatter']) ? $form_state['conf']['formatter_options'][$id]['field_formatter'] : NULL,
      );
    }
  }

  return $form;
}

/**
 * Submit formatter options form.
 */
function ctools_entity_fields_entity_fields_content_type_formatter_options_submit($form, &$form_state) {
  // Store in conf.
  foreach ($form_state['values']['fields'] as $id => $field) {
    $form_state['conf']['formatter_options'][$id] = $field;
  }
}

/**
 * Formatter settings form.
 */
function ctools_entity_fields_entity_fields_content_type_formatter_settings($form, &$form_state) {
  // Fields live in an array.
  $form['fields'] = array();

  // Fields are tree huggers.
  $form['fields']['#tree'] = TRUE;

  // Loop through enabled fields.
  foreach ($form_state['conf']['fields'] as $id => $field) {
    // Ensure that field is enabled.
    if ($field['enabled']) {
      // Get field info.
      $field_info = field_info_field($field['field_name']);

      // Create fieldset.
      $form['fields'][$id] = array(
        '#type'  => 'fieldset',
        '#title' => $field['field_label'],
      );

      // Load some Chaos Tools includes.
      ctools_form_include($form_state, 'fields');

      // Get view mode.
      $view_mode = 'ctools';

      // Get current settings from conf.
      $formatter_settings = isset($form_state['conf']['formatter_settings'][$id]) ? $form_state['conf']['formatter_settings'][$id] : array();

      // Get formatter.
      $formatter = field_info_formatter_types($form_state['conf']['formatter_options'][$id]['field_formatter']);

      // Add formatter defaults to current settings.
      if (isset($formatter['settings']) && is_array($formatter['settings'])) {
        $formatter_settings += $formatter['settings'];
      }

      // Ensure formatter settings form exists.
      if (function_exists($function = $formatter['module'] . '_field_formatter_settings_form')) {
        // Create fake instance of field.
        $instance = ctools_fields_fake_field_instance($field_info['field_name'], $view_mode, $form_state['conf']['formatter_options'][$id]['field_formatter'], $formatter_settings);

        // Get formatter settings form.
        if ($settings_form = $function($field_info, $instance, $view_mode, $form, $form_state)) {
          // Add elements from settings form.
          $form['fields'][$id] += $settings_form;
        }
      }

      // Cardinality is a funny word.
      if (isset($field_info['cardinality']) && $field_info['cardinality'] != 1) {
        // Add offset.
        list($prefix, $suffix) = explode('@count', t('Skip the first @count item(s)'));
        $form['fields'][$id]['delta_offset'] = array(
          '#type'          => 'textfield',
          '#size'          => 5,
          '#field_prefix'  => $prefix,
          '#field_suffix'  => $suffix,
          '#default_value' => isset($formatter_settings['delta_offset']) ? $formatter_settings['delta_offset'] : 0,
        );

        // Add limit.
        list($prefix, $suffix) = explode('@count', t('Then display at most @count item(s)'));
        $form['fields'][$id]['delta_limit'] = array(
          '#type'          => 'textfield',
          '#size'          => 5,
          '#field_prefix'  => $prefix,
          '#field_suffix'  => $suffix,
          '#description'   => t('Enter 0 to display all items.'),
          '#default_value' => isset($formatter_settings['delta_limit']) ? $formatter_settings['delta_limit'] : 0,
        );

        // Add reversed.
        $form['fields'][$id]['delta_reversed'] = array(
          '#type'          => 'checkbox',
          '#title'         => t('Display in reverse order'),
          '#description'   => t('(start from last values)'),
          '#default_value' => !empty($formatter_settings['delta_reversed']),
        );
      }

      // Hide if no settings.
      if (!element_children($form['fields'][$id])) {
        unset($form['fields'][$id]);
      }
    }
  }

  return $form;
}

/**
 * Submit formatter settings form.
 */
function ctools_entity_fields_entity_fields_content_type_formatter_settings_submit($form, &$form_state) {
  // Store in conf.
  foreach ($form_state['values']['fields'] as $id => $field) {
    $form_state['conf']['formatter_settings'][$id] = $field;
  }
}

/**
 * Render.
 */
function ctools_entity_fields_entity_fields_content_type_render($subtype, $conf, $panel_args, $contexts) {
  // Build content.
  $content = array();

  // Loop through fields.
  foreach ($conf['fields'] as $id => $field) {
    // Ensure that field is enabled.
    if (!$field['enabled']) {
      continue;
    }

    // Ensure context entity exists.
    if (empty($contexts[$field['context_id']]->data)) {
      continue;
    }

    // Get entity from context.
    $entity = clone $contexts[$field['context_id']]->data;

    // Ensure entity type is correct.
    $ids = entity_extract_ids($field['entity_type'], $entity);
    if ($ids[2] != $field['bundle_type']) {
      continue;
    }

    // Get display settings.
    $display = array(
      'label'    => !empty($conf['formatter_options'][$id]['field_label_display']) ? $conf['formatter_options'][$id]['field_label_display'] : '',
      'type'     => !empty($conf['formatter_options'][$id]['field_formatter']) ? $conf['formatter_options'][$id]['field_formatter'] : '',
      'settings' => !empty($conf['formatter_settings'][$id]) ? $conf['formatter_settings'][$id] : array(),
    );

    // Get field language.
    $language = field_language($field['entity_type'], $entity, $field['field_name']);

    // Get field values from entity.
    if (!is_array($values = field_get_items($field['entity_type'], $entity, $field['field_name'], $language))) {
      $values = array();
    }

    // Implement reverse order.
    if (!empty($conf['formatter_settings'][$id]['delta_reversed'])) {
      $values = array_reverse($values);
    }

    // Implement offset and limit.
    if (isset($conf['formatter_settings'][$id]['delta_limit'])) {
      // Get numbers.
      $limit  = $conf['formatter_settings'][$id]['delta_limit'];
      $offset = intval($conf['formatter_settings'][$id]['delta_offset']);
      $total  = count($values);

      // Fix limit.
      if ($limit == 0) {
        $limit = $total - $offset;
      }

      // Build new values.
      $new_values = array();
      for ($i = 0; $i < $limit; $i++) {
        $delta = $offset + $i;

        if (isset($values[$delta])) {
          $new_values[] = $values[$delta];
        }
      }

      // Implement new values.
      $values = $new_values;
    }

    // Set field values on entity.
    $entity->{$field['field_name']}[$language] = $values;

    // Add field to content.
    $content[$id] = field_view_field($field['entity_type'], $entity, $field['field_name'], $display);

    // Enforce field weight.
    $content[$id]['#weight'] = $field['weight'];
  }

  // Build block.
  $block = new stdClass();
  $block->title   = t('Multiple fields');
  $block->content = $content;
  $block->subtype = !empty($conf['subtype']) ? $conf['subtype'] : NULL;

  return $block;
}

